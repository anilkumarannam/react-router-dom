const Home = (props) => {
  const { name } = props;
  const homePage = <>
    <div className="home-container">
      <div className="home-content">
        <h1>Hi There !</h1>
        <h1>My Name is {name}</h1>
        <h1>I'm a Software Engineer.</h1>
      </div>
    </div>
  </>
  return homePage;
}

export default Home;