const About = (props) => {
  const aboutPage = <>
    <div className="profile">
      <div className="profile-content">
        <div className="image">
          <img alt="ProfilePicture" src="myPic.jpg" />
        </div>
        <div className="intro">
          <p>I've completed my graduation in Computer Science and Engineering from Kamala Institute of Technology and Science, Singapuram, Telangana
          </p>
        </div>
      </div>
    </div>
  </>
  return aboutPage;
}
export default About;