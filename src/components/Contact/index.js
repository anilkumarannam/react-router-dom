import { useState } from "react";

const Contact = (props) => {

  const [userName, setUserName] = useState({ username: "" });
  const [contactNumber, setContactNumber] = useState({ contact: "" });
  
  const controlUserName = (changeEvent) => {
    setUserName({ username: changeEvent.target.value });
  }
  const controlPhoneNumber = (changeEvent) => {
    setContactNumber({ contact: changeEvent.target.value });
  }

  const contactPage = <>
    <div className="contact">
      <div className="contact-content">
        <div className="contact-location">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.6022063509613!2d77.60984591541288!3d12.93326929088106!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae14526d47175f%3A0x8a51177fba6ca896!2s91springboard%207th%20Block%2C%20Koramangala!5e0!3m2!1sen!2sin!4v1651550906390!5m2!1sen!2sin" allowFullScreen="" loading="lazy" referrerPolicy="no-referrer-when-downgrade" title="abcd"></iframe>
        </div>
        <div className="contact-form">
          <form action="#">
            <label>User Name</label>
            <input type='text' name='username' placeholder="Name" required onChange={controlUserName} pattern="^[a-zA-Z ]+$" title="Please provide a valid name."></input>
            <label>Mobile Number</label>
            <input type='text' name='phonenumber' placeholder="Phone Number" required onChange={controlPhoneNumber} pattern="^[0-9]{10}$" title="Please provide a 10 digit mobile number"></input>
            <input type='submit' name="submit" value='submit'></input>
          </form>
        </div>
      </div>
    </div>
  </>
  return contactPage;
}

export default Contact;