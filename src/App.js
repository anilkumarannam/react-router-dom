import './App.css';
import RouteContainer from './components/RouteContainer';

function App() {
  return (
    <RouteContainer></RouteContainer>
  );
}

export default App;
